package buckethandler

// gcloud functions deploy HelloGCSInfo --runtime go111 --trigger-resource YOUR_TRIGGER_BUCKET_NAME --trigger-event google.storage.object.finalize

import (
	"context"
	"errors"
	"fmt"
	"image"
	"image/jpeg"
	"log"
	"os"
	"time"

	"github.com/nfnt/resize"

	"cloud.google.com/go/storage"
)

var client *storage.Client

// GCSEvent is the payload of a GCS event.
type GCSEvent struct {
	Bucket         string    `json:"bucket"`
	Name           string    `json:"name"`
	Metageneration string    `json:"metageneration"`
	ResourceState  string    `json:"resourceState"`
	TimeCreated    time.Time `json:"timeCreated"`
	Updated        time.Time `json:"updated"`
}

func CreateThumbnail(ctx context.Context, e GCSEvent) error {
	log.Printf("received new object with name: %s", e.Name)
	// Get the thumbnail bucket name
	tnbucket := os.Getenv("THUMBNAIL_BUCKET")
	if tnbucket == "" {
		return errors.New("THUMBNAIL_BUCKET not set")
	}

	// GCS client
	client, err := storage.NewClient(ctx)
	if err != nil {
		return fmt.Errorf("Error creating storage client: %s", err.Error())
	}
	// Get bucket reader for the new object
	r, err := client.Bucket(e.Bucket).Object(e.Name).NewReader(ctx)
	if err != nil {
		return fmt.Errorf("Error creating a reader for %s/%s: %s", e.Bucket, e.Name, err.Error())
	}
	defer r.Close()

	// Get image
	i, _, err := image.Decode(r)
	if err != nil {
		return fmt.Errorf("Error decoding the image: %s", err.Error())
	}

	// Resize the image.
	dst := resize.Resize(200, 0, i, resize.Lanczos3)

	// Create a writer for an object in the tnbucket with the original name
	w := client.Bucket(tnbucket).Object(e.Name).NewWriter(ctx)
	defer w.Close()

	// Write the image
	opt := &jpeg.Options{
		Quality: 100,
	}
	if err := jpeg.Encode(w, dst, opt); err != nil {
		return fmt.Errorf("Error encoding the image to %s/%s: %s", tnbucket, e.Name, err.Error())
	}
	log.Printf("Created thumbnail: %s/%s => %s/%s", e.Bucket, e.Name, tnbucket, e.Name)
	return nil
}
