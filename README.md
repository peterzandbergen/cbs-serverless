# Test commands

## Preparation

- Create a project
- Configure gcloud with the correct projectname and credentials
- Create the buckets in the project
- Enable the APIs
- Deploy the function
- Copy the images and show the thumbnails

### Create gcs project
Create a project in the console.


### Configure gcloud
Set the project and the authentication.

### Enable the apis
Execute in shell.
```
gcloud services enable \
    cloudfunctions.googleapis.com
```

### Create the buckets
```
gsutil mb gs://cbs-serverless-images
gsutil mb gs://cbs-serverless-images-thumbnails
```


## Deploy the function
cd naar buckethandler en execute in shell.
```
gcloud functions deploy CreateThumbnail \
    --runtime=go111 \
    --memory=128MB \
    --trigger-resource=cbs-serverless-images \
    --trigger-event=google.storage.object.finalize \
    --set-env-vars=THUMBNAIL_BUCKET=cbs-serverless-images-thumbnails
```

### Test the function
Use your own image instead of *Gopher.jpg*

```
gsutil cp Gopher.jpg gs://cbs-serverless-images/
```

### Show the result
Execute in shell
```
watch gsutil ls -l gs://cbs-serverless-images
watch gsutil ls -l gs://cbs-serverless-images-thumbnails
```

## Appendix

```
gcloud functions deploy HelloGCSInfo \
    --runtime=go111 \
    --trigger-resource=cbs-serverless-images \
    --trigger-event=google.storage.object.finalize
```

